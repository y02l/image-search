import json
import os

import pytest
import cv2

import run
from run import app

BASE_DIR = os.path.abspath(os.path.dirname(__file__))


@pytest.fixture(scope='function', autouse=True)
def prepost():
    app.config['TESTING'] = True
    yield
    pass


def test_post_search():
    client = app.test_client()
    img = cv2.imread(BASE_DIR + "/imgs/ヴァンパイア・ロード__1.png")
    b64 = run.img2base64str(img)
    resp = client.post('/search',
                       content_type='application/json',
                       data=json.dumps(dict(query=b64)))
    data = json.loads(resp.get_data().decode('UTF-8'))
    assert data[0]['tag'] == 'ヴァンパイア・ロード'


def test_post_regist():
    client = app.test_client()
    img = cv2.imread(BASE_DIR + "/imgs/ノーブル・ド・ノワール__1.png")
    b64 = run.img2base64str(img)
    resp = client.post('/regist',
                       content_type='application/json',
                       data=json.dumps(dict(img=b64, tag='ノーブル・ド・ノワール')))

    resp = client.post('/search',
                       content_type='application/json',
                       data=json.dumps(dict(query=b64)))

    data = json.loads(resp.get_data().decode('UTF-8'))
    assert data[0]['tag'] == 'ノーブル・ド・ノワール'
