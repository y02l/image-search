from .puzzle import signature, score


class Search():
    def __init__(self):
        self.signatures = []

    def regist(self, img, tag):
        '''
        Parameters
        ----------
        img : cv2.image
          クエリ画像
        tag : str
          検索結果として返すtag
        '''

        self.signatures.append(dict(signature=signature(img), tag=tag))

    def search(self, img):
        '''
        Parameters
        ----------
        img : cv2.image
          クエリ画像

        Returns
        -------
        [{tag, socre}, ...]
          類似順にソートされた非検索DB情報
        '''

        if len(self.signatures) == 0:
            return []
        q_sign = signature(img)
        res = [
            dict(tag=d['tag'], score=score(d['signature'], q_sign))
            for d in self.signatures
        ]
        res = sorted(res, key=lambda x: x['score'])
        return res


__all__ = [Search]
