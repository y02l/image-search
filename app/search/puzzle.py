import os

import cv2
from libpuzzle import Puzzle

REGIST_IMG_SIZE = 40


class PuzzleWrapper(Puzzle):
    '''
    cv2 imageを引数にできるようにしただけのwrapper
    '''
    def from_cv2image(self, img):
        cv2.imwrite('tmp.png', img)
        signature = self.from_filename('tmp.png')
        os.remove('tmp.png')
        return signature

    def from_filename(self, filename):
        if not os.path.exists(filename):
            raise FileNotFoundError(filename + ' is not found')
        return super(PuzzleWrapper, self).from_filename(filename)


def signature(img):
    img = cv2.resize(img, (REGIST_IMG_SIZE, REGIST_IMG_SIZE))
    puzzle = PuzzleWrapper()
    return puzzle.from_cv2image(img)


def score(a, b):
    return a.distance(b)
