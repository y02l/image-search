import io
import os
import re
import base64
from glob import glob
import numpy as np

import cv2
from flask import Flask, request, jsonify
from werkzeug.middleware.profiler import ProfilerMiddleware

from search import Search

BASE_DIR = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False
search = Search()

if app.debug:
    app.config['PROFILE'] = True
    app.wsgi_app = ProfilerMiddleware(app.wsgi_app,
                                      sort_by=['tottime'],
                                      restrictions=[20])


def path2tag(p):
    filename = os.path.basename(p)
    return re.sub('__.*', '', filename)


def init_regist():
    for img_file in glob(BASE_DIR + '/init_image_db/**/*', recursive=True):
        img = cv2.imread(img_file)
        if img is not None:
            search.regist(img, path2tag(img_file))


init_regist()


def img2base64str(img):
    retval, buffer = cv2.imencode('.png', img)
    png_base64 = base64.b64encode(buffer)
    png_str = png_base64.decode()
    return png_str


def base64str2img(b):
    b = b.split(',')[-1]
    # base64 -> binary
    img_binary = base64.b64decode(b)
    png = np.frombuffer(img_binary, dtype=np.uint8)
    # binary -> img
    img = cv2.imdecode(png, cv2.IMREAD_COLOR)
    return img


@app.route('/search', methods=['POST'])
def post_search():
    img = base64str2img(request.json['query'])
    return jsonify(search.search(img))


@app.route('/regist', methods=['POST'])
def post_regist():
    img = base64str2img(request.json['img'])
    tag = request.json['tag']
    search.regist(img, tag)
    return 'regist!', 200
