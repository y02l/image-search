FROM jjanzic/docker-python3-opencv
#FROM python:3.8-alpine

# for libpuzzle
RUN apt update
RUN apt install -y libpuzzle-dev
RUN apt install -y libgd-dev

COPY ./app/requirements.txt /app/
RUN pip install -r /app/requirements.txt

COPY ./app /app
COPY ./init_image_db /app/init_image_db

# start flask
ENV FLASK_APP='/app/run.py'
ENTRYPOINT ["flask", "run", "--host", "0.0.0.0", "--port", "80"]
