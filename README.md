# 画像検索に関するイメージ
クエリ画像と類似する画像を画像DB中から返すサービスです

機械学習ではないので、ノイズ等に弱いです。


## 初期画像DB
すでに画像DBが存在する場合、画像登録のリクエストを省略できます。

画像DBは```./init_image_db```の中に置いてください。

ファイル名は```{tag}__.*```としてください。


## APIリファレンス
### 画像検索
#### HTTPリクエスト
POST http://image-search/search

#### リクエストボディ
```
{
  query: base64
}
```


#### レスポンス
```
[
  {
    tag: str,
    score: int
  }
]
```

### 画像登録
#### HTTPリクエスト
POST http://image-search/regist

#### リクエストボディ

* img : base64 image
* tag : str : レスポンスで使われるtag

```
{
  img: base64,
  tag: str
}
```
